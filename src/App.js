import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'

import './App.css';
import AppNavBar from './components/AppNavBar';
import Products from './pages/Products'; 
import SpecificProduct from './pages/SpecificProduct'
import Home from './pages/Home';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import {UserProvider} from './UserContext'


function App() {
  
  //jwt proccessing as admin or not
  const [user, setUser] = useState({
  id:null,
  isaAdmin: null
  })

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !=='undefined'){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
      
    })
  }, [])
  console.log(user)
  // block to show whats on page main app body
  return (
    <UserProvider value={{user, setUser}}>
      <Router>
        <>
        <AppNavBar />
        <Container >
        <Switch>
          <Route exact path="/"component={Home}/>
          <Route exact path="/products"component={Products}/>
          <Route exact path="/products/:productId"component={SpecificProduct}/>
          <Route exact path="/cart/"component={Cart}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/logout" component={Logout}/>
          <Route exact path="/orders" component={Orders}/>
          <Route component ={Error}/>
          </Switch>
        </Container>
        </>
      </Router>
     </UserProvider> 
  );
}

export default App;



/*
requirements for capstone 3
register page ///
login page//
products catalog page//
.retrieve all products//
.reteie single products//
admin dashboard//
.create product//
.retriev ALL products//
. update product information//
.deactivate.ractievat product//
checkout order//
.non admin user check out(create order)//
*/