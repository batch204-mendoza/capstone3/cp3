import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Cart from '../pages/Cart';

export default function ProductCard({productsProp}) {


	const {_id, name, description, price} = productsProp;
	//values for when add to cart
	//const [cartItems,setCartItems]=useState([])



	 const quantity = 0
		return (
	    <Card className="mb-2">
	        <Card.Body className="d-flex flex-column">
	            <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
	            	<span className="fs-2">{name}</span>
				</Card.Title> 
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>PhP {price}</Card.Text>
{/*	            <Card.Text>Enrollees: {count}</Card.Text>
	            <Card.Text>Seats: {seats}</Card.Text>
	            <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
	        <span>
	            <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
	        </span>
	        <span>    
	            {/*<div className="mt-auto">
	                     
	                        <Button className="w-10" 
	                        onClick={() => addToCart()}
	                        >+ Add To Cart
	                        </Button>
	                  
	                      
       		    </div>*/}
	        </span>   

	        </Card.Body>
	    </Card>
	)
}