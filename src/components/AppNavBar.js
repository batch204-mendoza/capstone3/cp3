import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown, Button } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	//A context object such as our UserContext can be "opened" with React's useContext hook
	const { user } = useContext(UserContext);

	return(
		<Navbar className="bg-light shadow-sm mb-3" bg="light" expand="lg">
		  <Container> 
		    <Link className="navbar-brand" to="/">Manang's</Link>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
			{/*
				- className is use instead class, to specify a CSS class

				- changes for Bootstrap 5
				from mr -> to me
				from ml -> to ms
			*/}
		      <Nav className="">
		        <Link className="nav-link" to="/">Home</Link>
		        <Link className="nav-link" to="/products" exact>Products</Link>
		        <Link className="nav-link" to="/orders" exact>Orders</Link>


		        {(user.id !== null) ?
		        	<>
		        	<Link className="nav-link " to="/logout">Logout</Link>
		        	
		        	</>
		        	:
		        	<>
			        	<Link className="nav-link" to="/login">Login</Link>
			        	<Link className="nav-link" to="/register">Register</Link>
		        	</>
		        }

		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}