import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	
	// sonsider replacing this
	return(
		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2>Delivery anywhere</h2>
				        </Card.Title>
				        <Card.Text>
				            Our nationwide coverage of riders and branches will cater to all needs of every filipino from the Babuyan islands all the way to islands of Tawi-Tawi. 
				        </Card.Text>
				    </Card.Body>
				</Card>
				</Col>

				<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2>Delivery anytime</h2>
				        </Card.Title>
				        <Card.Text>
				            Neither snow nor rain nor heat nor gloom of night stays our couriers from the swift completion of their appointed and your satisfied bellies.
				        </Card.Text>
				    </Card.Body>
				</Card>
				</Col>
				
				<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				    <Card.Body>
				        <Card.Title>
				            <h2>Happiness Gurantee</h2>
				        </Card.Title>
				        <Card.Text>
				            Manangs food is so good that she has yet to have had a curstomer that has not had 100 percent Happiness ater partaking in her dishes. we carry on Mannangs spirit by offering to return all of the money spent on our meals if for any reason a customer is not happy with the meal the delivery the day or the climate. 
				        </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}