import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';

export default function AdminView(props){
	// console.log(props)

	//destructure the productsProp and the fetchData function from Products.js
	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	//states for handling modal visibility
	const [showEdit, setShowEdit] = useState(false)
	const [showEdit2, setShowEdit2]= useState(false)
	const token = localStorage.getItem("token")
// button for getting a specific product
	//Functions to handle opening and closing modals
	const openEdit = (productId) => {
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		setShowEdit(true)
	}

	const openEdit2 = () => {
			setProductId()
			setName()
			setDescription()
			setPrice()
		

		setShowEdit2(true)
	}

//fail safe for failure to update product
	const closeEdit = () => {
		setProductId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
	}
	const closeEdit2 = () => {
		setProductId("")
		setName("")
		setDescription("")
		setPrice(0)
		setShowEdit2(false)
	}
//update a specific product
	const editProduct = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully updated")
				//close the modal and set all states back to default values
				closeEdit()
				fetchData()
				//we call fetchData here to update the data we receive from the database
				//calling fetchData updates our coursesProp, which the useEffect below is monitoring
				//since the courseProp updates, the useEffect runs again, which re-renders our table with the updated data
			}else{
				alert("Something went wrong")
			}
		})
	}


//update a specific product
	const addProduct = (e) => {
		e.preventDefault()
		

		fetch(`${process.env.REACT_APP_API_URL}/products/addproducts`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("added product")

				closeEdit2()
				fetchData()
				
			}else{
				alert("Something went wrong")
			}
		})
	}



	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				let bool

				isActive ? bool = "disabled" : bool = "enabled"

				alert(`Product successfully ${bool}`)

				fetchData()
			}else{
				alert("Something went wrong")
			}
		})
	}

	useEffect(() => {
		//map through the ProductsProp to generate table contents
		const products = productsProp.map(products => {
			return(
				<tr key={products._id}>
					<td>{products.name}</td>
					<td>{products.description}</td>
					<td>{products.price}</td>
					<td>
							{/*Dynamically render products
							availability*/}
							{products.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(products._id)}>Update</Button>
						{products.isActive
							//dynamically render which button show depending on course availability
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(products._id, products.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(products._id, products.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		//set the CoursesArr state with the results of our mapping so that it can be used in the return statement
		setProductsArr(products)

	}, [productsProp])
	
	return(
		<>
			<h2>Admin Dashboard</h2>

			{/*Course info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the productsProp*/}
					{productsArr}
				</tbody>
			</Table>

			<Button variant="warning" size="sm" onClick={() => openEdit2()}>add product</Button>


			{/*Edit product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>


			{/*modal for add product*/}
			<Modal show={showEdit2} onHide={closeEdit2}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>add product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={name}
								onChange={e => setName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productsPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit2}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>





		</>
	)
}

// import { useState, useEffect } from 'react';
// import { Table, Button, Modal, Form } from 'react-bootstrap';

// export default function AdminView(props){

// 	//destructure the coursesProp and the fetchData function from Courses.js
// 	const { coursesProp, fetchData } = props;

// 	const [coursesArr, setCoursesArr] = useState([])
// 	const [courseId, setCourseId] = useState("")
// 	const [name, setName] = useState("")
// 	const [description, setDescription] = useState("")
// 	const [price, setPrice] = useState(0)
// 	//states for handling modal visibility
// 	const [showEdit, setShowEdit] = useState(false)

// 	const token = localStorage.getItem("token")

// 	//Functions to handle opening and closing modals
// 	const openEdit = (courseId) => {
// 		console.log(courseId)
// 		setShowEdit(true)
// 		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)
// 			//if you can call on the data then you can set the values
// 			setCourseId(data._id)
// 			setName(data.name)
// 			setDescription(data.Description)
// 			setPrice(data.price)
// 		})

// 	}

// 	const closeEdit = () => {
// 		// bug protection if may ginawang kalakohan with the modals
// 		setCourseId("")
// 		setName("")
// 		setDescription("")
// 		setPrice(0)
// 		setShowEdit(false)
// 	}

// 	const editCourse= ()=>{
// 		// prevent defualt response from reloading the page
// 		e.preventDefault()

// 		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`,{
// 			method: "PUT",
// 			headers: {
// 				"Content-Type": "application/json",
// 				Authorization: `Bearer ${token}`
// 			},
// 			body: JSON.stringify({
// 				name: name,
// 				description: description,
// 				price: price
// 			})
// 		})

// 	.then(res => res.json())
// 	.then(data=>{
// 		if(data){
// 			alert("course successfuly updated")
// 			// clsoe the modal and set all the staes back to the deaulf values
// 			closeEdit()
// 			fetchData()
// 			// we call fetch data here to update the date we receive from the databse
// 			// calling fetch daata updates our coursesProp, which the useeffct below is monitoring
// 			// since th courseprop udpets, the useffect rungs again,
// 			//which rerenders the useseffect our table with the updated data
// 		}
// 		else{
// 			alert("something went wrong")
// 		}
// 	  })
// 	}

// const archiveToggle=(courseId,isActive) => {
// 	fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`,{
// 		method: "PUT",
// 		headers: {
// 			"Content-Type": "application/json",
// 			Authorization: `Bearer ${token}`
// 		},
// 		body: JSON.stringify({
// 			isActive: !isActive
// 		})
// 	})
// 	.then(res => res.json())
// 	.then(data => {
// 		if (data){
// 			let bool
// 			isActive ? bool = "disabled" : bool ="enabled"

// 			alert(`Course successfuly ${bool}`)
// 			fetchData()
// 		}else{
// 			alert("something went wrong")
// 		}

// 	})
// }

// 	useEffect(() => {
// 		//map through the coursesProp to generate table contents
// 		const courses = coursesProp.map(course => {
// 			return(
// 				<tr key={course._id}>
// 					<td>{course.name}</td>
// 					<td>{course.description}</td>
// 					<td>{course.price}</td>
// 					<td>
// 							{/*Dynamically render course availability*/}
// 							{course.isActive
// 								? <span>Available</span>
// 								: <span>Unavailable</span>
// 							}
// 					</td>
// 					<td>
// 						<Button variant="primary" size="sm" onClick={() => openEdit(course._id)}>Update</Button>
// 						{course.isActive
// 							//dynamically render which button show depending on course availability
// 							? <Button variant="danger" size="sm" onClick={() =>archiveTogggle(course._id,course.isActice)}>Disable</Button>
// 							: <Button variant="success" size="sm" " onClick={()=>archiveTogggle(course._id,course.isActice)}>Enable</Button>
// 						}
// 					</td>
// 				</tr>
// 			)
// 		})

// 		//set the CoursesArr state with the results of our mapping so that it can be used in the return statement
// 		setCoursesArr(courses)

// 	}, [coursesProp])
	
// 	return(
// 		<>
// 			<h2>Admin Dashboard</h2>

// 			{/*Course info table*/}
// 			<Table striped bordered hover responsive>
// 				<thead className="bg-dark text-white">
// 					<tr>
// 						<th>Name</th>
// 						<th>Description</th>
// 						<th>Price</th>
// 						<th>Availability</th>
// 						<th>Actions</th>
// 					</tr>
// 				</thead>
// 				<tbody>
// 					{/*Mapped table contents dynamically generated from the coursesProp*/}
// 					{coursesArr}
// 				</tbody>
// 			</Table>

// 			{/*Edit Course Modal*/}
// 			<Modal show={showEdit} onHide={closeEdit}>
// 				e=>editCourse(e)>
// 					<Modal.Header closeButton>
// 						<Modal.Title>Update Course</Modal.Title>
// 					</Modal.Header>

// 					<Modal.Body>
// 						<Form.Group controlId="courseName">
// 							<Form.Label>Name</Form.Label>
// 							<Form.Control
// 								value={name}
// 								onChange={e => setName(e.target.value)}
// 								type="text"
// 								required
// 							/>
// 						</Form.Group>

// 						<Form.Group controlId="courseDescription">
// 							<Form.Label>Description</Form.Label>
// 							<Form.Control
// 								value={description}
// 								onChange={e => setDescription(e.target.value)}
// 								type="text"
// 								required
// 							/>
// 						</Form.Group>

// 						<Form.Group controlId="coursePrice">
// 							<Form.Label>Price</Form.Label>
// 							<Form.Control
// 								value={price}
// 								onChange={e => setPrice(e.target.value)}
// 								type="number"
// 								required
// 							/>
// 						</Form.Group>
// 					</Modal.Body>

// 					<Modal.Footer>
// 						<Button variant="secondary" onClick={closeEdit}>Close</Button>
// 						<Button variant="success" type="submit">Submit</Button>
// 					</Modal.Footer>
// 				</Form>
// 			</Modal>
// 		</>
// 	)
// }
