import React, { useState, useEffect } from 'react';
import { Container, Accordion, InputGroup, Button, FormControl, Table } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom';
//import SpecificProduct from '../pages/SpecificProduct';
export default function Cart(){
	const [total, setTotal] = useState(0)
	const [cart, setCart] = useState([])
	const [tableRows, setTableRows] = useState([])
	const [willRedirect, setWillRedirect] = useState(false)
// useffect to load/render localstorage info into a usable array
	useEffect(()=> {
		console.log("test1")
		if(localStorage.getItem('cart')){
			setCart(JSON.parse(localStorage.getItem('cart')))
			console.log("test2")
			console.log(localStorage)
			console.log(localStorage.cart)
			
	
		}
	}, [])
// useeffect to render the items into a table based on the local storage details.
	useEffect(()=> {

		let cartItems = cart.map((item, index) => {
			console.log(item)
			console.log("test3")
			console.log(cart.Item)
			console.log(item.productId)

			console.log(item.name)
			console.log(item.quantity)

			console.log(item.quantity)
							

			return ( 
		      <tr key={item.productId}>
		          <td colSpan="2"><Link  to={`/products/${item.productId}`}>{item.name}</Link></td>
		          <td colSpan="1">₱{item.price}</td>
		          <td>
				  	<InputGroup className="d-md-none">
						<FormControl type="number" min="1" value={item.quantity} onChange={e => qtyInput(item.productId, e.target.value)}/>
					</InputGroup>
					<InputGroup className="d-none d-md-flex w-50">
						
						{/*<InputGroup.Prepend>
*/}							<Button variant="secondary" onClick={() => qtyBtns(item.productId, "-")}>-</Button>
						{/*</InputGroup.Prepend>*/}
						<FormControl type="number" min="1" value={item.quantity} onChange={e => qtyInput(item.productId, e.target.value)}/>
						{/*<InputGroup.Append>
*/}							<Button variant="secondary" onClick={() => qtyBtns(item.productId, "+")}>+</Button>
{/*						</InputGroup.Append>*/}
					</InputGroup>
		          </td>
		          <td>₱{item.subtotal}</td>
		          <td className="text-center">
		          	<Button variant="danger" onClick={() => removeBtn(item.productId)}>Remove</Button>
		          </td>
		      </tr>			
			)
		})
		console.log("test4")
		// set states for the table above depending on the cart array. console log works but set tablerow does not
	
		setTableRows(cartItems)
		console.log(cartItems)

		let tempTotal = 0


		cart.forEach((item)=> {
			tempTotal += item.subtotal
		})
		console.log(tempTotal)
	
	

		setTotal(tempTotal)
	}, [cart])

		

	const qtyInput = (productId, value) => {

		let tempCart = [...cart]
		console.log("test8")
	
		if(value === ''){
			value = 1
			console.log("test9")
	
		}else if(value === "0"){
			console.log("test10")
	
			alert("Quantity can't be lower than 1.")
			value = 1

		}
		console.log("test11")
	
		for(let i = 0; i < cart.length; i++){
			console.log("test12")
	
			if(tempCart[i].productId === productId){
				tempCart[i].quantity = parseFloat(value)
				tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
			}
		}
		console.log("test13")
		console.log(tempCart)
		setCart(tempCart)
		console.log(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}

	const qtyBtns = (productId, operator) => {

		let tempCart = [...cart]

		for(let i = 0; i < tempCart.length; i++){

			if(tempCart[i].productId === productId){
				if(operator === "+"){
					tempCart[i].quantity += 1
					tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
				}else if(operator === "-"){
					if(tempCart[i].quantity <= 1){
						alert("Quantity can't be lower than 1.")
					}else{
						tempCart[i].quantity -= 1
						tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity
					}
				}
			}
		}

		setCart(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))
	}

	const removeBtn = (productId) => {
		let tempCart = [...cart]

		let cartIds = tempCart.map((item)=> {
			return item.productId
		})

		tempCart.splice([cartIds.indexOf(productId)], 1)

		setCart(tempCart)
		localStorage.setItem('cart', JSON.stringify(tempCart))	
	}

	const checkout = () => {
		console.log("checkout")
		const checkoutCart =  cart.map((item) => {
			console.log("checkout")
			console.log(item.name)
			return {
				productName: item.name,
				productId: item.productId,
				quantity: item.quantity,

			}
		})
		console.log("checkout")

		fetch(`${ process.env.REACT_APP_API_URL}/users/addToCart`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				products: checkoutCart,
				totalAmount: total
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				alert("Order placed! Thank you!")

				localStorage.removeItem('cart')

				setWillRedirect(true)
			}else{
				alert("Something went wrong. Order was NOT placed.")
			}
		})
	}
	console.log(total)
	return(
		willRedirect === true
		? <Redirect to="/orders"/>
		:
		cart.length <= 0
			? <Accordion>
				<h3 className="text-center">Your cart is empty! <Link to="/products">Start shopping.</Link></h3>
			</Accordion>
			:
			<>
				<h2 className="text-center my-4">Cart</h2>
				<Table striped bordered hover responsive>
					<thead className="bg-secondary text-white">
						<tr>
							<th colSpan="2">Name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Subtotal</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						
						{tableRows}
					
						
						
						<tr>
							<td colSpan="5">
								<h3>Total: ₱{total}</h3>
							</td>
							<td colSpan="1">
								<Button variant="success" block onClick={()=> checkout()}>Checkout</Button>
							</td>
						</tr>
					</tbody>						
				</Table>
			</>
	)
}