import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import Cart from '../pages/Cart';
import UserContext from '../UserContext';
// destructured the localtion of params into match so we can pull course id right away right away
export default function SpecificProduct({match}) {
	//console.log(props)
	const { user } = useContext(UserContext)
	const [id, setId] = useState("")
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const productId = match.params.productId;
	const [qty, setQty] = useState(1)
//	console.log(courseId)

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res=> res.json())
		.then(data=>{
			//why is it this way review
			setId(data._id)
			setName(data.name)
			setPrice(data.price)
			setDescription(data.Description)
			//console.log(data)
		})
	},[])

	const reduceQty = () => {
		if(qty <= 1){
			alert("Quantity can't be lower than 1.")
		}else{
			setQty(qty - 1)
		}
	}

	 const addToCart = () => {
			let alreadyInCart = false
			let productIndex
			let message
			let cart = []

			if(localStorage.getItem('cart')){
				cart = JSON.parse(localStorage.getItem('cart'))
			}

			for(let i = 0; i < cart.length; i++){
				if(cart[i].productId === id){
					alreadyInCart = true
					productIndex = i
				}
			}

			if(alreadyInCart){
				cart[productIndex].quantity += qty
				cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity
			}else{
				cart.push({
					'productId' : id,
					'name': name,
					'price': price,
					'quantity': qty,
					'subtotal': price * qty
				})		
			}

			localStorage.setItem('cart', JSON.stringify(cart))

			if(qty === 1){
				message = "1 item added to cart."
			}else{
				message = `${qty} items added to cart.`
			}

			alert(message)
		}

		const qtyInput = (value) => {
			if(value === ''){
				value = 1
			}else if(value === "0"){
				alert("Quantity can't be lower than 1.")
				value = 1
			}

			setQty(value)
		}

	return(
		<Container className="mb-5">
			
			<Link className="btn btn-primary"size="sm" to={`/products`}>return to menu</Link>
			<Card>
				<Card.Body className="text-center">
					<Card.Subtitle>Name:</Card.Subtitle>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>PhP {price}</Card.Text>
				</Card.Body>
				<Card.Footer>
				{user.id !== null
					? user.isAdmin === true
						? <Button variant="danger" block disabled>Admin can't Add to Cart</Button>
						: <Button variant="primary" block onClick={addToCart}>Add to Cart</Button>
					: <Link className="btn btn-warning btn-block" to={{pathname: '/login', state: { from: 'cart'}}}>Log in to Add to Cart</Link>
				}
	      		</Card.Footer>
			</Card>
			
		</Container>
	)
}

