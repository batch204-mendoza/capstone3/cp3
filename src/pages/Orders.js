import React, { useState, useEffect } from 'react';
import { Container, Card, Accordion } from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default function Orders(){

	const [ordersList, setOrdersList] = useState([])


	useEffect(()=> {

		fetch(`${ process.env.REACT_APP_API_URL}/users/userorders`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log("data")
			console.log(data)
			if(data.length > 0){
				let orders = data.map((item, index)=> {
					return(
						<Card key={item._id}>
			 				
			 					<Card.Body>
									<h6>Purhcased on:  <span className="text-warning">{item.purchasedOn }</span></h6>
			 						<ul>
									{
										item.products.map((subitem) => {
											return (
												<li key={subitem.productId}>{subitem.productName} - Quantity: {subitem.quantity}</li>
											)
										})
									}
									</ul>
			 						<h6>Total: <span className="text-warning">₱{item.totalAmount}</span></h6>
			 					</Card.Body>
			 				
			 			</Card>
			 		)
			 	})
	
			 	setOrdersList(orders)				
			 }
		})
	}, [])

	return(
		ordersList.length === 0
		?<Accordion>
				<h3 className="text-center">No orders placed yet! <Link to="/products">Start shopping.</Link></h3>
		</Accordion>
		:
		<Container>
			<h2 className="text-center my-4">Order History</h2>
			<Accordion>
				{ordersList}
			</Accordion>
		</Container>
	)
}