import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){

	const data = {
		title: "Mannangs",
		content: "Ulam for everyone, everywhere, everytime!",
		destination: "/products",
		label: "Order Now!"
	}

	return(
		<>
			<Banner dataProp={data}/>
			<Highlights />
		</>
	)
}